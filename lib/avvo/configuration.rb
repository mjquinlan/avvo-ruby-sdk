require 'base64'

module Avvo
  class Configuration
    attr_accessor :email, :password, :debug

    def authorization
      raise Avvo::ImproperCredentials.new unless @email && @password

      unless @authorization
        encoded = Base64.encode64("#{@email}:#{@password}").strip
        @authorization = "Basic #{encoded}"
      end

      @authorization
    end
  end
end
