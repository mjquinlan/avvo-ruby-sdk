module Avvo
  class Model
    attr_accessor :attributes

    def initialize(attributes)
      @attributes = attributes
    end

    def self.base_path
      raise Avvo::MethodNotImplemented.new 'Model.base_url'
    end

    def self.find(id, params={})
      Avvo.client.get "#{base_path}/#{id}", params
    end
  end
end
