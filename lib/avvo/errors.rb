module Avvo
  class NotConfigured < StandardError
    def initialize
      super("You must configure the avvo gem")
    end
  end

  class ImproperCredentials < StandardError
    def initialize
      super("E-mail and/or password must be defined in the Avvo configuration")
    end
  end

  class MethodNotImplemented < StandardError
    def initialize(method)
      super("You have to implement #{method}")
    end
  end
end
