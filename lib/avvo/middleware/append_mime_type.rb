module Avvo
  module Middleware
    class AppendMimeType < Faraday::Middleware
      def call(request_env)
        path = request_env[:url].path
        request_env[:url].path = "#{path}.json" unless path.end_with?('.json')

        @app.call(request_env)
      end
    end
  end
end
