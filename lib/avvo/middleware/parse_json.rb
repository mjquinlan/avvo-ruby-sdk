module Avvo
  module Middleware
    class ParseJson < Faraday::Middleware
      def call(env)

        @app.call(env).on_complete do |env|
          env[:body] = Oj.load(env[:body]) if env[:body]
        end
      end
    end
  end
end
