module Avvo
  class Lawyer < Model
    def self.base_path
      'lawyers'
    end

    def self.search(params={})
      Avvo.client.get "#{base_path}/search", params
    end

    def self.reviews(id, params={})
      Avvo.client.get "#{base_path}/#{id}/reviews", params
    end

    def self.headshot(id, params={})
      Avvo.client.get "#{base_path}/#{id}/headshot"
    end

    def self.schools(id, params={})
      Avvo.client.get "#{base_path}/#{id}/schools", params
    end

    def self.specialties(id, params={})
      Avvo.client.get "#{base_path}/#{id}/specialties", params
    end

    def self.addresses(id, params={})
      Avvo.client.get "#{base_path}/#{id}/addresses", params
    end

    def self.main_address(id, params={})
      Avvo.client.get "#{base_path}/#{id}/addresses/main", params
    end
  end
end
