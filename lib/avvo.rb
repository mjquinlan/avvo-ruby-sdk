require "faraday"
require "oj"

require "avvo/version"

require "avvo/errors"
require "avvo/configuration"
require "avvo/middleware/append_mime_type"
require "avvo/middleware/parse_json"

require "avvo/model"
require "avvo/lawyer"

module Avvo
  ENDPOINT = "https://api.avvo.com/api/1"

  def self.config
    @config || raise(Avvo::NotConfigured.new)
  end

  def self.configure
    @config ||= Avvo::Configuration.new

    yield @config
  end

  def self.client
    @client ||= Faraday.new ENDPOINT do |f|
      f.use Avvo::Middleware::AppendMimeType
      f.use Avvo::Middleware::ParseJson
      f.headers['Authorization'] = config.authorization
      f.request :url_encoded
      f.response :logger if Avvo.config.debug
      f.adapter Faraday.default_adapter
    end
  end
end
